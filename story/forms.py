from django import forms
from django.contrib.auth.models import User
# from .models import UserModel
from django.db import models

class UserRegisterForm(forms.ModelForm):
	
	class Meta:
		model = User
		fields = ['username', 'email','password']
		widgets = {
            'username': forms.TextInput(attrs={'placeholder': 'Username'}),
            'email': forms.TextInput(attrs={'placeholder': 'Email', 'required': True}),
            'password': forms.PasswordInput(attrs={'placeholder': 'Password'}),
        }
