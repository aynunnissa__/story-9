from django.test import LiveServerTestCase, TestCase, tag, Client
from . views import register, userLogin
from django.contrib.auth.models import User

class StoryTestCase(TestCase):
	def test_url_to_register(self):
		request = Client().get('/register/')
		self.assertEquals(request.status_code, 200)

	def test_url_to_login(self):
		request = Client().get('/login/')
		self.assertEquals(request.status_code, 200)

	def test_url_to_home(self):
		request = Client().get('/')
		self.assertEquals(request.status_code, 200)

	def test_user_register(self):
		data = {'username': 'user','email': 'user@gmail.com', 'password': 'user123'}
		request = Client().post('/register/')
		request.method= 'POST'
		request.POST = data
		hasil = register(request)
		hasil.client = Client()
		self.assertRedirects(hasil, '/login/')

	def test_homepage_user(self):
		user = User.objects.create_user('user', 'user@thebeatles.com', 'user123')
		request = Client().get('/')
		request.user = user
		self.assertEquals(request.status_code, 200)
		