from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from .forms import UserRegisterForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required


def register(request):
	if request.method == 'POST' :
		form = UserRegisterForm(request.POST)
		if form.is_valid():
			username = form.cleaned_data.get('username')
			email = form.cleaned_data.get('email')
			password = form.cleaned_data.get('password')
			User.objects.create_user(username, email,password)
			# messages.success(request, f'Your account has been created! You are now able to log in')
			return redirect('story9:userLogin')
	else :
		form = UserRegisterForm()
	return render(request, 'story9/register.html', {'form' : form})


def userLogin(request):
	if request.method == 'POST' :
		username = request.POST['username']
		password = request.POST['password1']
		user = authenticate(request, username=username, password=password)
		if user is not None:
			login(request,user)
			return redirect('story9:homepage')
		else:
			messages.error(request, f'invalid username or password')
	elif request.user.is_authenticated:
		return redirect('story9:homepage')
	return render(request, 'story9/login.html')

def userLogout(request):
	logout(request)
	return redirect('story9:userLogin')

def homepage(request):
	if request.user.is_authenticated :
		user = request.user.username
		return render(request, 'story9/homepage.html', {'name': user, 'log': 1})
	return render(request, 'story9/homepage.html')