from django.urls import path

from . import views

app_name = 'story9'

urlpatterns = [
    path('login/', views.userLogin, name='userLogin'),
    path('register/', views.register, name='register'),
    path('', views.homepage, name='homepage'),
    path('logout/', views.userLogout, name='userLogout'),
]
